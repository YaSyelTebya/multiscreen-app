# Android Navigation App
This is an Android application that implements a navigation flow between four screens. Each screen has a unique title and two buttons: Go Forward and Go Back. By tapping on the Go Forward button, the next screen is opened. By tapping on the Go Back button, the current screen is closed and the previous screen is opened.

## Features
Four screens with unique titles and navigation buttons.
Proper navigation flow from the first screen to the last screen and vice versa.
Logging of lifecycle events for each screen.

## Getting Started
Clone the repository to your local machine using the following command:
```
git clone https://gitlab.com/YaSyelTebya/multiscreen-app.git
```
Open the project in Android Studio.

Build and run the app on an Android device or emulator.
## Usage
Once the app is running, you can navigate between screens by tapping on the Go Forward and Go Back buttons. The app will log the lifecycle events for each screen in the Logcat.