package com.example.multiscreenapp

import android.content.Intent
import android.widget.Button
import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity

abstract class GoBackAndForwardActivity(@IdRes protected val nextScreenButtonId: Int,
                                        @IdRes protected val previousScreenButtonId: Int,
                                        protected val nextScreen: Class<out AppCompatActivity>,
                                        private val previousScreen: Class<out AppCompatActivity>?
    ) : AppCompatActivity() {

    protected fun initNavigationBehaviour() {
        if(isGoingHome()) {
            onCalledToGoHome()
        }
        val goToNextActivityButton = findViewById<Button>(nextScreenButtonId)
        val goToPreviousActivityButton = findViewById<Button>(previousScreenButtonId)

        setupButtons(goToNextActivityButton, goToPreviousActivityButton)
    }

    private fun setupButtons(goToNextActivityButton: Button, goToPreviousActivityButton: Button) {
        goToNextActivityButton.setOnClickListener {
            goToNextScreen()
        }

        goToPreviousActivityButton.setOnClickListener {
            goToPreviousScreen()
        }
    }

    protected fun onCalledToGoHome(
        screenToReturnTo: Class<out AppCompatActivity>? = previousScreen) {
        screenToReturnTo ?: return

        val goHomeIntent = Intent(this, screenToReturnTo)
        goHomeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        goHomeIntent.putExtra(GOING_HOME_INTENT_EXTRA_NAME, true)

        startActivity(goHomeIntent)
    }

    private fun isGoingHome(): Boolean = intent.getBooleanExtra(
        GOING_HOME_INTENT_EXTRA_NAME,
        false)

    protected open fun goToNextScreen() {
        val goToNextScreenIntent = Intent(this, nextScreen)
        startActivity(goToNextScreenIntent)
    }

    private fun goToPreviousScreen() = this.finish()

    companion object {
        const val GOING_HOME_INTENT_EXTRA_NAME = "isGoingHome"
    }
}