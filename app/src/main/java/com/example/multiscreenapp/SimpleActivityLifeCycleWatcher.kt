package com.example.multiscreenapp

import android.util.Log
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner

class SimpleActivityLifeCycleWatcher : DefaultLifecycleObserver {
    override fun onCreate(owner: LifecycleOwner) {
        Log.i(LOG_TAG, "$owner created")
    }

    override fun onDestroy(owner: LifecycleOwner) {
        Log.i(LOG_TAG, "$owner destroyed")
    }

    override fun onStart(owner: LifecycleOwner) {
        Log.i(LOG_TAG, "$owner started")
    }

    override fun onPause(owner: LifecycleOwner) {
        Log.i(LOG_TAG, "$owner paused")
    }

    override fun onResume(owner: LifecycleOwner) {
        Log.i(LOG_TAG, "$owner resumed")
    }

    override fun onStop(owner: LifecycleOwner) {
        Log.i(LOG_TAG, "$owner stopped")
    }

    companion object {
        private const val LOG_TAG = "MultiscreenApp"
    }
}