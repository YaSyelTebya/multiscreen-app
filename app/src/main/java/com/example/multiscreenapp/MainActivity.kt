package com.example.multiscreenapp

import android.os.Bundle

class MainActivity : GoBackAndForwardActivity(
    R.id.main_activity_go_to_next_activity_button,
    R.id.main_activity_go_to_previous_activity_button,
    SecondActivity::class.java,
     null
) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initNavigationBehaviour()
        lifecycle.addObserver(SimpleActivityLifeCycleWatcher())
    }
}