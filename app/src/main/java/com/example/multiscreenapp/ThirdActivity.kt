package com.example.multiscreenapp

import android.os.Bundle

class ThirdActivity : GoBackAndForwardActivity(
    R.id.third_activity_go_to_next_activity_button,
    R.id.third_activity_go_to_previous_activity_button,
    FourthActivity::class.java,
    SecondActivity::class.java,
) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_third)
        initNavigationBehaviour()
        lifecycle.addObserver(SimpleActivityLifeCycleWatcher())
    }
}