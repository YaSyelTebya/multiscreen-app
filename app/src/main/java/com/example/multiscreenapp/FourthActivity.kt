package com.example.multiscreenapp

import android.os.Bundle

class FourthActivity : GoBackAndForwardActivity(
    R.id.fourth_activity_go_to_next_activity_button,
    R.id.fourth_activity_go_to_previous_activity_button,
    MainActivity::class.java,
    ThirdActivity::class.java
) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fourth)
        initNavigationBehaviour()
        lifecycle.addObserver(SimpleActivityLifeCycleWatcher())
    }

    override fun goToNextScreen() = onCalledToGoHome(nextScreen)
}