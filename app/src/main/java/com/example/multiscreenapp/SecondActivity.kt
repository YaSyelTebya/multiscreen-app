package com.example.multiscreenapp

import android.os.Bundle

class SecondActivity : GoBackAndForwardActivity(
    R.id.second_activity_go_to_next_activity_button,
    R.id.second_activity_go_to_previous_activity_button,
    ThirdActivity::class.java,
    null
) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        initNavigationBehaviour()
        lifecycle.addObserver(SimpleActivityLifeCycleWatcher())
    }
}